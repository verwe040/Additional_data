#!/usr/bin/python

from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio.SeqUtils import GC123
from Bio import SeqIO

input_file = open('C:\\input_file.Fasta, 'r')
output_file = open('C:\\output_file.tsv', 'w')

output_file.write('GC\tGC1\tGC2\tGC3\n')

for cur_record in SeqIO.parse(input_file, "fasta") :
    gene_name = cur_record.name
    A_count = cur_record.seq.count('A')
    T_count = cur_record.seq.count('T') 
    C_count = cur_record.seq.count('C')
    G_count = cur_record.seq.count('G')
    length = len(cur_record.seq)
    GC_content = GC123(cur_record.seq)
    #print GC_content
    output_line = '%f\%f\%f\%f\n' % \
    (GC_content)
    output_file.write(output_line) 

output_file.close() 
input_file.close()

