﻿#Command Lines used to plot the bimodal distribution
    #The chosen input file contained the GC and GC3 values against their percentage in the CDS
    #This example presents the GC3 plot, different parameters in stat_smooth were used for the GC plot
>GC3 <- read.table(choose.files(), h=T)
>ggplot(GC3, aes(GC3, CDS))
+geom_point()
+stat_smooth(method='lm',formula=y~poly(x^2,4),size=2,color='red')
+ggtitle(“B”)

#Command Lines used for randomization
    #The chosen input file contained all the loci of the CDS and their GC3 values
>my.data <- as.numeric(as.vector(read.table(choose.files())[,1]))
>replicate(1000, mean(sample(my.data, 590)))

#Command Lines used to plot normal distribution + observed mean
    #The chosen input files were 1) ND: data of randomization and 2) NDobs: data containing observed mean
>ND <- read.table(choose.files(), h=T)
>NDobs <- read.table(choose.files(), h=T)
>ggplot(ND,aes(GC3,Appearance))
+geom_point()
+geom_smooth(color="red",size=3)
+stat_function(fun=dnorm,color="grey")
+geom_point(data=NDobs,shape=21,color="black",fill="yellow",size=3,stroke=3)

#Command Lines used to plot GC3 content of loci harbouring mPing above the general GC3 bimodal distribution
	#The chosen input files were 1) GC3plotmPing: The GC3 content of 590 loci harbouring mPing and 2) GC3 values of all loci in CDS
>GC3plotmPing <- read.table(choose.files(),h=T)
>BDGC3A <- read.table(choose.files(),h=T)
>d1 <- GC3plotmPing 
>d2 <- BDGC3A
>d1$panel <- "a"
>d2$panel <- "b"
>d1$z <- d1$x
>d <- rbind(d1,d2)
>ggplot(data=d,mapping=aes(GC3, Appearance))
+facet_grid(panel~., scale="free")
+stat_smooth(data=d1, method='lm',formula=y~poly(x^2,6),size=1.5,color='red')
+stat_smooth(data=d2, method='lm',formula=y~poly(x^4,8),color='red',size=1.5)
+geom_point(data=d1, size=1)
+geom_point(data=d2, size=1)

